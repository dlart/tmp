<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/** @var ClassLoader $classLoader */
$classLoader = require __DIR__.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader([$classLoader, 'loadClass']);

return $classLoader;
