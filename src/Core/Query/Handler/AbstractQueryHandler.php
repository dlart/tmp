<?php

namespace Todomer\Core\Query\Handler;

use Assert\Assertion;
use Todomer\Core\Query\Handler\QueryHandlerInterface as QueryHandler;
use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * AbstractQueryHandler.
 */
abstract class AbstractQueryHandler implements QueryHandler
{
    /**
     * @param Query $query
     *
     * @return QueryResult
     */
    public function handle(Query $query): QueryResult
    {
        $queryName = ClassFunctions::short($query);

        $handleMethodName = 'handle'.$queryName;

        Assertion::methodExists($handleMethodName, $this);

        return $this->$handleMethodName($queryName);
    }
}
