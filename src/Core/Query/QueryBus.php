<?php

namespace Todomer\Core\Query;

use Assert\Assertion;
use Todomer\Core\Query\Handler\QueryHandlerInterface as QueryHandler;
use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * QueryBus.
 */
class QueryBus
{
    /**
     * @var QueryHandler[]
     */
    private $handlers = [];

    /**
     * @param Query $query
     *
     * @return QueryResult
     */
    public function handle(Query $query): QueryResult
    {
        $fullyQualifiedQueryClassName = ClassFunctions::fqcn($query);

        Assertion::keyIsset($this->handlers, $fullyQualifiedQueryClassName);

        return $this->handlers[$fullyQualifiedQueryClassName]->handle($query);
    }

    /**
     * @param QueryHandler $queryHandler
     * @param string       $fullyQualifiedQueryClassName
     */
    public function registerHandler(
        QueryHandler $queryHandler,
        string $fullyQualifiedQueryClassName
    ): void {
        Assertion::classExists($fullyQualifiedQueryClassName);

        $this->handlers[$fullyQualifiedQueryClassName] = $queryHandler;
    }
}
