<?php

namespace Todomer\Core\Aggregate\Root;

use Todomer\Core\HasIdentityInterface as HasIdentity;

/**
 * AggregateRootInterface.
 */
interface AggregateRootInterface extends HasIdentity
{
}
