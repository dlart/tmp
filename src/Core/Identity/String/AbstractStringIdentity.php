<?php

namespace Todomer\Core\Identity\String;

use Todomer\Core\Identity\AbstractIdentity as Identity;

/**
 * AbstractStringIdentity.
 */
abstract class AbstractStringIdentity extends Identity
{
}
