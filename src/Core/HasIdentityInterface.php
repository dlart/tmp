<?php

namespace Todomer\Core;

use Todomer\Core\Identity\IdentityInterface as Identity;

/**
 * HasIdentityInterface.
 */
interface HasIdentityInterface
{
    /**
     * @return Identity
     */
    public function getIdentity(): Identity;
}
