<?php

namespace Todomer\Core\ValueObject;

use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * ValueObjectInterface.
 */
interface ValueObjectInterface
{
    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool;
}
