<?php

namespace Todomer\Core\ValueObject;

use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * AbstractValueObject.
 */
abstract class AbstractValueObject implements ValueObject
{
    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return $this instanceof static;
    }
}
