<?php

namespace Todomer\Core;

use Carbon\Carbon as DateTime;
use Todomer\Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * Moment.
 */
class Moment extends AbstractValueObject implements CanBeCastedToString
{
    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @param string $string
     *
     * @return Moment
     */
    public static function createFromString(string $string): Moment
    {
        return new self(
            DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $string
            )
        );
    }

    /**
     * @return Moment
     */
    public static function createFromNow(): Moment
    {
        return new self(DateTime::now());
    }

    /**
     * @param DateTime $dateTime
     */
    private function __construct(
        DateTime $dateTime
    ) {
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $this->dateTime->toDateTimeString();
    }

    /**
     * @return int
     */
    public function getDay(): int
    {
        return $this->dateTime->day;
    }

    /**
     * @return int
     */
    public function getHour(): int
    {
        return $this->dateTime->hour;
    }

    /**
     * @return int
     */
    public function getMinute(): int
    {
        return $this->dateTime->minute;
    }

    /**
     * @return int
     */
    public function getMonth(): int
    {
        return $this->dateTime->month;
    }

    /**
     * @return int
     */
    public function getSecond(): int
    {
        return $this->dateTime->second;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->dateTime->year;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        /* @var self $valueObject */
        return
            parent::isEqualTo($valueObject)
            && $this->getYear() === $valueObject->getYear()
            && $this->getMonth() === $valueObject->getMonth()
            && $this->getDay() === $valueObject->getDay()
            && $this->getHour() === $valueObject->getHour()
            && $this->getMinute() === $valueObject->getMinute()
            && $this->getSecond() === $valueObject->getSecond()
        ;
    }
}
