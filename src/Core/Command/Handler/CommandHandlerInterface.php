<?php

namespace Todomer\Core\Command\Handler;

use Todomer\Core\Command\CommandInterface as Command;

/**
 * CommandHandlerInterface.
 */
interface CommandHandlerInterface
{
    /**
     * @param Command $command
     */
    public function handle(Command $command): void;
}
