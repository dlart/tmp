<?php

namespace Todomer\Core\Command\Handler;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Handler\CommandHandlerInterface as CommandHandler;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * AbstractCommandHandler.
 */
abstract class AbstractCommandHandler implements CommandHandler
{
    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $commandName = ClassFunctions::short($command);

        $handleMethodName = 'handle'.$commandName;

        Assertion::methodExists($handleMethodName, $this);

        $this->$handleMethodName($command);
    }
}
