<?php

namespace Todomer\Core\Command\Exception;

use RuntimeException;

/**
 * CommandRejectedException.
 */
class CommandRejectedException extends RuntimeException
{
}
