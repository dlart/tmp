<?php

namespace Todomer\Core\Command\Validator;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Validator\CommandValidatorInterface as CommandValidator;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * AbstractCommandValidator.
 */
abstract class AbstractCommandValidator implements CommandValidator
{
    /**
     * @param Command $command
     */
    public function validate(Command $command): void
    {
        $validateMethodName = 'validate'.ClassFunctions::short($command);

        Assertion::methodExists($validateMethodName, $this);

        $this->$validateMethodName($command);
    }
}
