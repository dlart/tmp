<?php

namespace Todomer\Core\Command\Validator;

use Todomer\Core\Command\CommandInterface as Command;

/**
 * CommandValidatorInterface.
 */
interface CommandValidatorInterface
{
    /**
     * @param Command $command
     */
    public function validate(Command $command): void;
}
