<?php

namespace Todomer\Core\Command;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Exception\CommandRejectedException;
use Todomer\Core\Command\Handler\CommandHandlerInterface as CommandHandler;
use Todomer\Core\Command\Validator\CommandValidatorInterface as CommandValidator;
use Verraes\ClassFunctions\ClassFunctions;

/**
 * CommandBus.
 */
class CommandBus
{
    /**
     * @var CommandHandler[]
     */
    private $handlers = [];

    /**
     * @var CommandValidator[]
     */
    private $validators = [];

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $fullyQualifiedCommandClassName = ClassFunctions::fqcn($command);

        try {
            if (isset($this->validators[$fullyQualifiedCommandClassName])) {
                $this->validators[$fullyQualifiedCommandClassName]->validate($command);
            }
        } catch (CommandRejectedException $commandRejectedException) {
            return;
        }

        Assertion::keyIsset($this->handlers, $fullyQualifiedCommandClassName);

        $this->handlers[$fullyQualifiedCommandClassName]->handle($command);
    }

    /**
     * @param CommandHandler $commandHandler
     * @param string         $fullyQualifiedCommandClassName
     */
    public function registerHandler(
        CommandHandler $commandHandler,
        string $fullyQualifiedCommandClassName
    ): void {
        Assertion::classExists($fullyQualifiedCommandClassName);

        $this->handlers[$fullyQualifiedCommandClassName] = $commandHandler;
    }

    /**
     * @param CommandValidator $commandValidator
     * @param string           $fullyQualifiedCommandClassName
     */
    public function registerValidator(
        CommandValidator $commandValidator,
        string $fullyQualifiedCommandClassName
    ): void {
        Assertion::classExists($fullyQualifiedCommandClassName);

        $this->validators[$fullyQualifiedCommandClassName] = $commandValidator;
    }
}
