<?php

namespace Todomer\Core;

use Todomer\Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * Username.
 */
class Username extends AbstractValueObject implements CanBeCastedToString
{
    /**
     * @var string
     */
    private $username;

    /**
     * @param string $username
     */
    public function __construct(string $username)
    {
        $this->assertThatUsernameIsValid($username);

        $this->username;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return
            parent::isEqualTo($valueObject)
            && $this->normalizeUsernameBeforeCompare((string) $this)
            === $this->normalizeUsernameBeforeCompare((string) $valueObject)
        ;
    }

    /**
     * @param string $username
     */
    protected function assertThatUsernameIsValid(string $username): void
    {
    }

    /**
     * @param string $username
     *
     * @return string
     */
    protected function normalizeUsernameBeforeCompare(string $username): string
    {
        return $username;
    }
}
