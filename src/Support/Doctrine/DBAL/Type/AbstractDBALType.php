<?php

namespace Todomer\Support\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Doctrine\DBAL\Types\Type as DBALType;

/**
 * AbstractDBALType.
 */
abstract class AbstractDBALType extends DBALType
{
    /**
     * @return string
     */
    abstract public static function getNameInStaticContext(): string;

    /**
     * @param Platform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(Platform $platform): bool
    {
        return true;
    }
}
