<?php

namespace Todomer\Support\Symfony\Component\HTTPKernel\Bundle;

use Doctrine\DBAL\Types\Type as DbalType;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * AbstractBundle.
 */
abstract class AbstractBundle extends Bundle
{
    public function boot(): void
    {
        $this->registerDbalTypes();
    }

    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function build(ContainerBuilder $containerBuilder): void
    {
        foreach ($this->getFullyQualifiedCompilerPassesClassNames() as $fullyQualifiedCompilerPassClassName) {
            $containerBuilder->addCompilerPass(
                new $fullyQualifiedCompilerPassClassName()
            );
        }
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedCompilerPassesClassNames(): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedDbalTypesClassNames(): array
    {
        return [];
    }

    private function registerDbalTypes(): void
    {
        foreach ($this->getFullyQualifiedDbalTypesClassNames() as $fullyQualifiedDbalTypeClassName) {
            if (DbalType::hasType(call_user_func([$fullyQualifiedDbalTypeClassName, 'getNameInStaticContext']))) {
                continue;
            }

            DbalType::addType(
                call_user_func([$fullyQualifiedDbalTypeClassName, 'getNameInStaticContext']),
                $fullyQualifiedDbalTypeClassName
            );

            $this
                ->container
                ->get('doctrine.orm.entity_manager')
                ->getConnection()
                ->getDatabasePlatform()
                ->registerDoctrineTypeMapping(
                    call_user_func([$fullyQualifiedDbalTypeClassName, 'getNameInStaticContext']),
                    call_user_func([$fullyQualifiedDbalTypeClassName, 'getNameInStaticContext'])
                )
            ;
        }
    }
}
