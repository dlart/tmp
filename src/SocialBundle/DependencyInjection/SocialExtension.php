<?php

namespace Todomer\SocialBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class SocialExtension extends Extension
{
    /**
     * @param array            $config
     * @param ContainerBuilder $containerBuilder
     */
    public function load(
        array $config,
        ContainerBuilder $containerBuilder
    ): void {
        $loader = new YamlFileLoader(
            $containerBuilder,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');
    }
}
