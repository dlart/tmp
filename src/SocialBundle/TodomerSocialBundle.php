<?php

namespace Todomer\SocialBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Todomer\SocialBundle\DependencyInjection\SocialExtension;

/**
 * TodomerSocialBundle.
 */
class TodomerSocialBundle extends Bundle
{
    /**
     * @return SocialExtension
     */
    public function getContainerExtension(): SocialExtension
    {
        return new SocialExtension();
    }
}
