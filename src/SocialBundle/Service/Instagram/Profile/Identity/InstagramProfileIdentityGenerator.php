<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Identity\Generator;

use Todomer\CoreBundle\Service\Generator\String\UUID\UUIDGeneratorInterface as UUIDGenerator;
use Todomer\Social\Instagram\Profile\Identity\InstagramProfileIdentityGeneratorInterface;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * InstagramProfileIdentityGenerator.
 */
class InstagramProfileIdentityGenerator implements InstagramProfileIdentityGeneratorInterface
{
    /**
     * @var UUIDGenerator
     */
    private $uuidGenerator;

    /**
     * @param UUIDGenerator $uuidGenerator
     */
    public function __construct(UUIDGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @return InstagramProfileIdentity
     */
    public function generate(): InstagramProfileIdentity
    {
        return new InstagramProfileIdentity(
            $this->uuidGenerator->generate()
        );
    }
}
