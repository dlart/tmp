<?php

namespace Todomer\Social\Instagram\Profile\Query\Handler\IsInstagramProfileOfIdentityExist;

use Todomer\CoreBundle\Service\Query\Handler\DBAL\AbstractDBALQueryHandler;
use Todomer\Social\Instagram\Profile\Query\IsInstagramProfileOfIdentityExistQuery;
use Todomer\Social\Instagram\Profile\Query\Result\IsInstagramProfileOfIdentityExistQueryResult;

/**
 * DBALIsInstagramProfileOfIdentityExistQueryHandler.
 */
class DBALIsInstagramProfileOfIdentityExistQueryHandler extends AbstractDBALQueryHandler
{
    /**
     * @param IsInstagramProfileOfIdentityExistQuery $isInstagramProfileOfIdentityExistQuery
     *
     * @return IsInstagramProfileOfIdentityExistQueryResult
     */
    protected function handleIsInstagramProfileOfIdentityExistQuery(
        IsInstagramProfileOfIdentityExistQuery $isInstagramProfileOfIdentityExistQuery
    ): IsInstagramProfileOfIdentityExistQueryResult {
        return new IsInstagramProfileOfIdentityExistQueryResult(
            (bool) $this
                ->connection
                ->createQueryBuilder()
                ->select('identity')
                ->from('instagram_profile', 'ip')
                ->where('ip.identity = :identity')
                ->setParameter(
                    'identity',
                    (string) $isInstagramProfileOfIdentityExistQuery->getInstagramProfileIdentity()
                )
                ->getFirstResult()
        );
    }
}
