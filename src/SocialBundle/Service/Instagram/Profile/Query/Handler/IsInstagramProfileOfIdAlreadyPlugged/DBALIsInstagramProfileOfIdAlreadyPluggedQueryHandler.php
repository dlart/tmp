<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Query\Handler\IsInstagramProfileOfIdAlreadyPlugged;

use Todomer\CoreBundle\Service\Query\Handler\DBAL\AbstractDBALQueryHandler as DBALQueryHandler;
use Todomer\Social\Instagram\Profile\Query\IsInstagramProfileOfIdAlreadyPluggedQuery;
use Todomer\Social\Instagram\Profile\Query\Result\IsInstagramProfileOfIdAlreadyPluggedQueryResult;

/**
 * DBALIsInstagramProfileOfIdAlreadyPluggedQueryHandler.
 */
class DBALIsInstagramProfileOfIdAlreadyPluggedQueryHandler extends DBALQueryHandler
{
    /**
     * @param IsInstagramProfileOfIdAlreadyPluggedQuery $isInstagramProfileOfIdAlreadyPluggedQuery
     *
     * @return IsInstagramProfileOfIdAlreadyPluggedQueryResult
     */
    protected function handleIsInstagramProfileOfIdAlreadyPluggedQuery(
        IsInstagramProfileOfIdAlreadyPluggedQuery $isInstagramProfileOfIdAlreadyPluggedQuery
    ): IsInstagramProfileOfIdAlreadyPluggedQueryResult {
        return new IsInstagramProfileOfIdAlreadyPluggedQueryResult(
            (bool) $this
                ->connection
                ->createQueryBuilder()
                ->select('ip.id')
                ->from('instagram_profile', 'ip')
                ->where('ip.id = :id')
                ->setParameter(
                    'id',
                    (string) $isInstagramProfileOfIdAlreadyPluggedQuery->getInstagramProfileId()
                )
                ->getFirstResult()
        );
    }
}
