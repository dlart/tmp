<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Snapshot\Identity;

use Todomer\CoreBundle\Service\Generator\String\UUID\UUIDGeneratorInterface as UUIDGenerator;
use Todomer\Social\Instagram\Profile\Snapshot\Identity\Generator\InstagramProfileSnapshotIdentityGeneratorInterface;
use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity;

/**
 * InstagramProfileSnapshotIdentityGenerator.
 */
class InstagramProfileSnapshotIdentityGenerator implements InstagramProfileSnapshotIdentityGeneratorInterface
{
    /**
     * @var UUIDGenerator
     */
    private $uuidGenerator;

    /**
     * @param UUIDGenerator $uuidGenerator
     */
    public function __construct(UUIDGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @return InstagramProfileSnapshotIdentity
     */
    public function generate(): InstagramProfileSnapshotIdentity
    {
        return new InstagramProfileSnapshotIdentity(
            $this->uuidGenerator->generate()
        );
    }
}
