<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Metrics\Provider;

use InstagramAPI\Instagram as Client;
use Todomer\Social\Instagram\Profile\InstagramProfileId;
use Todomer\Social\Instagram\Profile\InstagramProfileMetrics;
use Todomer\Social\Instagram\Profile\Metrics\Provider\InstagramProfileMetricsProviderInterface as InstagramProfileMetricsProvider;

/**
 * Mgp25InstagramProfileMetricsProvider.
 */
class Mgp25InstagramProfileMetricsProvider implements InstagramProfileMetricsProvider
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param InstagramProfileId $instagramProfileId
     *
     * @return InstagramProfileMetrics
     */
    public function getByInstagramProfileId(
        InstagramProfileId $instagramProfileId
    ): InstagramProfileMetrics {
        $response = $this->client->getUserInfoById((string) $instagramProfileId);

        return new InstagramProfileMetrics(
            $response->user->follower_count,
            $response->user->following_count,
            $response->user->media_count,
            $response->user->profile_pic_url,
            $response->user->username
        );
    }
}
