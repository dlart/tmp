<?php

namespace Todomer\CoreBundle\Service\Generator\String\UUID;

/**
 * UUIDGeneratorInterface.
 */
interface UUIDGeneratorInterface
{
    /**
     * @return string
     */
    public function generate(): string;
}
