<?php

namespace Todomer\CoreBundle\Service\Generator\String\UUID;

use Ramsey\Uuid\Uuid;
use Todomer\CoreBundle\Service\Generator\String\UUID\UUIDGeneratorInterface as UUIDGenerator;

/**
 * RamseyUUIDGenerator.
 */
class RamseyUUIDGenerator implements UUIDGenerator
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return (string) Uuid::uuid4();
    }
}
