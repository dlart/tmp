<?php

namespace Todomer\CoreBundle\Service\Query\Handler\DBAL;

use Doctrine\DBAL\Connection;
use Todomer\Core\Query\Handler\AbstractQueryHandler as QueryHandler;

/**
 * AbstractDBALQueryHandler.
 */
abstract class AbstractDBALQueryHandler extends QueryHandler
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
}
