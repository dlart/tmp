<?php

namespace Todomer\CoreBundle;

use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\CommandHandlerCompilerPass;
use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\CommandValidatorCompilerPass;
use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\QueryHandlerCompilerPass;
use Todomer\CoreBundle\DependencyInjection\CoreExtension;
use Todomer\CoreBundle\Doctrine\DBAL\Type\MomentDBALType;
use Todomer\Support\Symfony\Component\HTTPKernel\Bundle\AbstractBundle as Bundle;

/**
 * TodomerCoreBundle.
 */
class TodomerCoreBundle extends Bundle
{
    /**
     * @return CoreExtension
     */
    public function getContainerExtension(): CoreExtension
    {
        return new CoreExtension();
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedCompilerPassesClassNames(): array
    {
        return [
            CommandHandlerCompilerPass::class,
            CommandValidatorCompilerPass::class,
            QueryHandlerCompilerPass::class,
        ];
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedDbalTypesClassNames(): array
    {
        return [
            MomentDBALType::class,
        ];
    }
}
