<?php

namespace Todomer\CoreBundle\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Todomer\Core\Moment;
use Todomer\Support\Doctrine\DBAL\Type\AbstractDBALType as DBALType;

/**
 * MomentDBALType.
 */
class MomentDBALType extends DBALType
{
    const NAME = 'moment';

    /**
     * @return string
     */
    public static function getNameInStaticContext(): string
    {
        return self::NAME;
    }

    /**
     * @param mixed    $value
     * @param Platform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, Platform $platform): string
    {
        return (string) $value;
    }

    /**
     * @param mixed    $value
     * @param Platform $platform
     *
     * @return Moment
     */
    public function convertToPHPValue($value, Platform $platform): Moment
    {
        return Moment::createFromString($value);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }

    /**
     * @param array    $fieldDeclaration
     * @param Platform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(
        array $fieldDeclaration,
        Platform $platform
    ): string {
        return $platform->getDateTimeTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @param Platform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(Platform $platform): bool
    {
        return true;
    }
}
