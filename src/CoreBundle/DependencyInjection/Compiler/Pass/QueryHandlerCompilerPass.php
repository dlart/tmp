<?php

namespace Todomer\CoreBundle\DependencyInjection\Compiler\Pass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * QueryHandlerCompilerPass.
 */
class QueryHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        if (!$containerBuilder->has('todomer.core.query.bus')) {
            return;
        }

        $definition = $containerBuilder->findDefinition('todomer.core.query.bus');

        $taggedServices = $containerBuilder
            ->findTaggedServiceIds('todomer.core.query.handler')
        ;

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerHandler',
                    [
                        new Reference($id),
                        $attributes['query'],
                    ]
                );
            }
        }
    }
}
