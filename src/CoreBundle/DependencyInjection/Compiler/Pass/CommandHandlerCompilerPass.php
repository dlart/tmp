<?php

namespace Todomer\CoreBundle\DependencyInjection\Compiler\Pass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * CommandHandlerCompilerPass.
 */
class CommandHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        if (!$containerBuilder->has('todomer.core.command.bus')) {
            return;
        }

        $definition = $containerBuilder->findDefinition('todomer.core.command.bus');

        $taggedServices = $containerBuilder
            ->findTaggedServiceIds('todomer.core.command.handler')
        ;

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerHandler',
                    [
                        new Reference($id),
                        $attributes['command'],
                    ]
                );
            }
        }
    }
}
