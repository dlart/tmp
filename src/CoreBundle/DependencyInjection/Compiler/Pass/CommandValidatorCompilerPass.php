<?php

namespace Todomer\CoreBundle\DependencyInjection\Compiler\Pass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * CommandValidatorCompilerPass.
 */
class CommandValidatorCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        if (!$containerBuilder->has('todomer.core.command.bus')) {
            return;
        }

        $definition = $containerBuilder->findDefinition('todomer.core.command.bus');

        $taggedServices = $containerBuilder
            ->findTaggedServiceIds('todomer.core.command.validator')
        ;

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerValidator',
                    [
                        new Reference($id),
                        $attributes['command'],
                    ]
                );
            }
        }
    }
}
