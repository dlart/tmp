<?php

namespace Todomer\CoreBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * CoreExtension.
 */
class CoreExtension extends Extension
{
    /**
     * @param array            $config
     * @param ContainerBuilder $containerBuilder
     */
    public function load(
        array $config,
        ContainerBuilder $containerBuilder
    ): void {
        $yamlFileLoader = new YamlFileLoader(
            $containerBuilder,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $yamlFileLoader->load('services.yml');
    }
}
