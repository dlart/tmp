<?php

namespace Todomer\Social\Instagram\Profile\Exception;

use RuntimeException;

/**
 * InstagramProfileNotFoundException.
 */
class InstagramProfileNotFoundException extends RuntimeException
{
}
