<?php

namespace Todomer\Social\Instagram\Profile;

use Assert\Assertion;
use Todomer\Core\Username;

/**
 * InstagramProfileUsername.
 */
class InstagramProfileUsername extends Username
{
    /**
     * @param string $username
     */
    protected function assertThatUsernameIsValid(string $username): void
    {
        Assertion::regex($username, '/[\.\-\_0-9a-z]/i');
    }

    /**
     * @param string $username
     *
     * @return string
     */
    protected function normalizeUsernameBeforeCompare(string $username): string
    {
        return strtolower($username);
    }
}
