<?php

namespace Todomer\Social\Instagram\Profile\Repository;

use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity as Identity;

/**
 * InstagramProfileRepositoryInterface.
 */
interface InstagramProfileRepositoryInterface
{
    /**
     * @param InstagramProfile $instagramProfile
     */
    public function add(InstagramProfile $instagramProfile): void;

    /**
     * @param Identity $identity
     *
     * @return null|InstagramProfile
     */
    public function findByIdentity(Identity $identity): ?InstagramProfile;
}
