<?php

namespace Todomer\Social\Instagram\Profile;

use Todomer\Core\Identity\String\Uuid\AbstractUuidIdentity as UuidIdentity;

/**
 * InstagramProfileIdentity.
 */
class InstagramProfileIdentity extends UuidIdentity
{
}
