<?php

namespace Todomer\Social\Instagram\Profile\Identity;

use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * InstagramProfileIdentityGeneratorInterface.
 */
interface InstagramProfileIdentityGeneratorInterface
{
    /**
     * @return InstagramProfileIdentity
     */
    public function generate(): InstagramProfileIdentity;
}
