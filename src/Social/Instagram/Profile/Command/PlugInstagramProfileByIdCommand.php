<?php

namespace Todomer\Social\Instagram\Profile\Command;

use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Social\Instagram\Profile\InstagramProfileId;

/**
 * PlugInstagramProfileByIdCommand.
 */
class PlugInstagramProfileByIdCommand implements Command
{
    /**
     * @var InstagramProfileId
     */
    private $instagramProfileId;

    /**
     * @param InstagramProfileId $instagramProfileId
     */
    public function __construct(InstagramProfileId $instagramProfileId)
    {
        $this->instagramProfileId = $instagramProfileId;
    }

    /**
     * @return InstagramProfileId
     */
    public function getInstagramProfileId(): InstagramProfileId
    {
        return $this->instagramProfileId;
    }
}
