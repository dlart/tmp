<?php

namespace Todomer\Social\Instagram\Profile\Command;

use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * UpdateInstagramProfileMetricsCommand.
 */
class UpdateInstagramProfileMetricsCommand implements Command
{
    /**
     * @var InstagramProfileIdentity
     */
    private $instagramProfileIdentity;

    /**
     * @param InstagramProfileIdentity $instagramProfileIdentity
     */
    public function __construct(InstagramProfileIdentity $instagramProfileIdentity)
    {
        $this->instagramProfileIdentity = $instagramProfileIdentity;
    }

    /**
     * @return InstagramProfileIdentity
     */
    public function getInstagramProfileIdentity(): InstagramProfileIdentity
    {
        return $this->instagramProfileIdentity;
    }
}
