<?php

namespace Todomer\Social\Instagram\Profile\Command\Validator;

use Todomer\Core\Query\QueryBus;
use Todomer\Social\Instagram\Profile\Command\UpdateInstagramProfileMetricsCommand;
use Todomer\Social\Instagram\Profile\Exception\InstagramProfileNotFoundException;
use Todomer\Social\Instagram\Profile\Query\IsInstagramProfileOfIdentityExistQuery;
use Todomer\Social\Instagram\Profile\Query\Result\IsInstagramProfileOfIdentityExistQueryResult;

/**
 * UpdateInstagramProfileMetricsCommandValidator.
 */
class UpdateInstagramProfileMetricsCommandValidator
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param UpdateInstagramProfileMetricsCommand $updateInstagramProfileMetricsCommand
     */
    protected function validateUpdateInstagramProfileMetricsCommand(
        UpdateInstagramProfileMetricsCommand $updateInstagramProfileMetricsCommand
    ): void {
        /** @var IsInstagramProfileOfIdentityExistQueryResult $isInstagramProfileOfIdentityExistQueryResult */
        $isInstagramProfileOfIdentityExistQueryResult = $this
            ->queryBus
            ->handle(
                new IsInstagramProfileOfIdentityExistQuery(
                    $updateInstagramProfileMetricsCommand->getInstagramProfileIdentity()
                )
            )
        ;

        if (!$isInstagramProfileOfIdentityExistQueryResult->isExist()) {
            throw new InstagramProfileNotFoundException(
                sprintf(
                    'Instagram profile of "%s" identity not found.',
                    (string) $updateInstagramProfileMetricsCommand->getInstagramProfileIdentity()
                )
            );
        }
    }
}
