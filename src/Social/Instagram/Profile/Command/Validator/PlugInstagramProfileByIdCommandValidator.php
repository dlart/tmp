<?php

namespace Todomer\Social\Instagram\Profile\Command\Validator;

use Todomer\Core\Command\Exception\CommandRejectedException;
use Todomer\Core\Command\Validator\AbstractCommandValidator as CommandValidator;
use Todomer\Core\Query\QueryBus;
use Todomer\Social\Instagram\Profile\Command\PlugInstagramProfileByIdCommand;
use Todomer\Social\Instagram\Profile\Query\IsInstagramProfileOfIdAlreadyPluggedQuery;
use Todomer\Social\Instagram\Profile\Query\Result\IsInstagramProfileOfIdAlreadyPluggedQueryResult;

/**
 * PlugInstagramProfileByIdCommandValidator.
 */
class PlugInstagramProfileByIdCommandValidator extends CommandValidator
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
     */
    protected function validatePlugInstagramProfileByIdCommand(
        PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
    ): void {
        /** @var IsInstagramProfileOfIdAlreadyPluggedQueryResult $isInstagramProfileOfIdAlreadyPluggedQueryResult */
        $isInstagramProfileOfIdAlreadyPluggedQueryResult = $this
            ->queryBus
            ->handle(
                new IsInstagramProfileOfIdAlreadyPluggedQuery(
                    $plugInstagramProfileByIdCommand->getInstagramProfileId()
                )
            )
        ;

        if ($isInstagramProfileOfIdAlreadyPluggedQueryResult->isAlreadyPlugged()) {
            throw new CommandRejectedException();
        }
    }
}
