<?php

namespace Todomer\Social\Instagram\Profile\Command\Handler;

use Todomer\Core\Command\Handler\AbstractCommandHandler as CommandHandler;
use Todomer\Social\Instagram\Profile\Command\UpdateInstagramProfileMetricsCommand;
use Todomer\Social\Instagram\Profile\InstagramProfileSnapshot;
use Todomer\Social\Instagram\Profile\Metrics\Provider\InstagramProfileMetricsProviderInterface as InstagramProfileMetricsProvider;
use Todomer\Social\Instagram\Profile\Repository\InstagramProfileRepositoryInterface as InstagramProfileRepository;
use Todomer\Social\Instagram\Profile\Snapshot\Identity\Generator\InstagramProfileSnapshotIdentityGeneratorInterface as InstagramProfileSnapshotIdentityGenerator;
use Todomer\Social\Instagram\Profile\Snapshot\Repository\InstagramProfileSnapshotRepositoryInterface as InstagramProfileSnapshotRepository;

/**
 * UpdateInstagramProfileMetricsCommandHandler.
 */
class UpdateInstagramProfileMetricsCommandHandler extends CommandHandler
{
    /**
     * @var InstagramProfileMetricsProvider
     */
    private $instagramProfileMetricsProvider;

    /**
     * @var InstagramProfileRepository
     */
    private $instagramProfileRepository;

    /**
     * @var InstagramProfileSnapshotIdentityGenerator
     */
    private $instagramProfileSnapshotIdentityGenerator;

    /**
     * @var InstagramProfileSnapshotRepository
     */
    private $instagramProfileSnapshotRepository;

    /**
     * @param InstagramProfileMetricsProvider           $instagramProfileMetricsProvider
     * @param InstagramProfileRepository                $instagramProfileRepository
     * @param InstagramProfileSnapshotIdentityGenerator $instagramProfileSnapshotIdentityGenerator
     * @param InstagramProfileSnapshotRepository        $instagramProfileSnapshotRepository
     */
    public function __construct(
        InstagramProfileMetricsProvider $instagramProfileMetricsProvider,
        InstagramProfileRepository $instagramProfileRepository,
        InstagramProfileSnapshotIdentityGenerator $instagramProfileSnapshotIdentityGenerator,
        InstagramProfileSnapshotRepository $instagramProfileSnapshotRepository
    ) {
        $this->instagramProfileMetricsProvider = $instagramProfileMetricsProvider;
        $this->instagramProfileRepository = $instagramProfileRepository;
        $this->instagramProfileSnapshotIdentityGenerator = $instagramProfileSnapshotIdentityGenerator;
        $this->instagramProfileSnapshotRepository = $instagramProfileSnapshotRepository;
    }

    /**
     * @param UpdateInstagramProfileMetricsCommand $updateInstagramProfileMetricsCommand
     */
    public function handleUpdateInstagramProfileMetricsCommand(
        UpdateInstagramProfileMetricsCommand $updateInstagramProfileMetricsCommand
    ): void {
        $instagramProfile = $this
            ->instagramProfileRepository
            ->findByIdentity(
                $updateInstagramProfileMetricsCommand->getInstagramProfileIdentity()
            )
        ;

        $instagramProfile->updateMetrics(
            $this
                ->instagramProfileMetricsProvider
                ->getByInstagramProfileId(
                    $instagramProfile->getId()
                )
        );

        $this->instagramProfileRepository->add($instagramProfile);

        $instagramProfileSnapshot = InstagramProfileSnapshot::snapFromInstagramProfile(
            $this->instagramProfileSnapshotIdentityGenerator->generate(),
            $instagramProfile
        );

        $this->instagramProfileSnapshotRepository->add($instagramProfileSnapshot);
    }
}
