<?php

namespace Todomer\Social\Instagram\Profile\Command\Handler;

use Todomer\Core\Command\Handler\AbstractCommandHandler as CommandHandler;
use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\Command\PlugInstagramProfileByIdCommand;
use Todomer\Social\Instagram\Profile\Identity\InstagramProfileIdentityGeneratorInterface as InstagramProfileIdentityGenerator;
use Todomer\Social\Instagram\Profile\Repository\InstagramProfileRepositoryInterface as InstagramProfileRepository;

/**
 * PlugInstagramProfileByIdCommandHandler.
 */
class PlugInstagramProfileByIdCommandHandler extends CommandHandler
{
    /**
     * @var InstagramProfileIdentityGenerator
     */
    private $instagramProfileIdentityGenerator;

    /**
     * @var InstagramProfileRepository
     */
    private $instagramProfileRepository;

    /**
     * @param InstagramProfileIdentityGenerator $instagramProfileIdentityGenerator
     * @param InstagramProfileRepository        $instagramProfileRepository
     */
    public function __construct(
        InstagramProfileIdentityGenerator $instagramProfileIdentityGenerator,
        InstagramProfileRepository $instagramProfileRepository
    ) {
        $this->instagramProfileIdentityGenerator = $instagramProfileIdentityGenerator;
        $this->instagramProfileRepository = $instagramProfileRepository;
    }

    /**
     * @param PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
     */
    protected function handlePlugInstagramProfileByIdCommand(
        PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
    ): void {
        $instagramProfile = InstagramProfile::plug(
            $this->instagramProfileIdentityGenerator->generate(),
            $plugInstagramProfileByIdCommand->getInstagramProfileId()
        );

        $this->instagramProfileRepository->add($instagramProfile);
    }
}
