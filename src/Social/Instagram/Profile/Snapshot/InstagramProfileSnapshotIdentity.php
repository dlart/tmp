<?php

namespace Todomer\Social\Instagram\Profile\Snapshot;

use Todomer\Core\Identity\String\Uuid\AbstractUuidIdentity as UuidIdentity;

/**
 * InstagramProfileSnapshotIdentity.
 */
class InstagramProfileSnapshotIdentity extends UuidIdentity
{
}
