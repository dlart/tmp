<?php

namespace Todomer\Social\Instagram\Profile\Snapshot\Identity\Generator;

use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity;

/**
 * InstagramProfileSnapshotIdentityGeneratorInterface.
 */
interface InstagramProfileSnapshotIdentityGeneratorInterface
{
    /**
     * @return InstagramProfileSnapshotIdentity
     */
    public function generate(): InstagramProfileSnapshotIdentity;
}
