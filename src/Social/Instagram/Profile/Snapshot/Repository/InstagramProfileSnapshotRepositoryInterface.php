<?php

namespace Todomer\Social\Instagram\Profile\Snapshot\Repository;

use Todomer\Social\Instagram\Profile\InstagramProfileSnapshot;

/**
 * InstagramProfileSnapshotRepositoryInterface.
 */
interface InstagramProfileSnapshotRepositoryInterface
{
    /**
     * @param InstagramProfileSnapshot $instagramProfileSnapshot
     */
    public function add(InstagramProfileSnapshot $instagramProfileSnapshot): void;
}
