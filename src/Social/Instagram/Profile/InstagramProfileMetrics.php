<?php

namespace Todomer\Social\Instagram\Profile;

use Todomer\Core\Url;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * InstagramProfileMetrics.
 */
class InstagramProfileMetrics extends AbstractValueObject
{
    /**
     * @var int
     */
    private $countOfFollowers;

    /**
     * @var int
     */
    private $countOfFollows;

    /**
     * @var int
     */
    private $countOfMedia;

    /**
     * @var Url
     */
    private $pictureUrl;

    /**
     * @var InstagramProfileUsername
     */
    private $username;

    /**
     * @param int                      $countOfFollowers
     * @param int                      $countOfFollows
     * @param int                      $countOfMedia
     * @param Url                      $pictureUrl
     * @param InstagramProfileUsername $username
     */
    public function __construct(
        int $countOfFollowers = null,
        int $countOfFollows = null,
        int $countOfMedia = null,
        Url $pictureUrl = null,
        InstagramProfileUsername $username = null
    ) {
        $this->countOfFollowers = $countOfFollowers;
        $this->countOfFollows = $countOfFollows;
        $this->countOfMedia = $countOfMedia;
        $this->pictureUrl = $pictureUrl;
        $this->username = $username;
    }

    /**
     * @return int
     */
    public function getCountOfFollowers(): ?int
    {
        return $this->countOfFollowers;
    }

    /**
     * @return int
     */
    public function getCountOfFollows(): ?int
    {
        return $this->countOfFollows;
    }

    /**
     * @return int
     */
    public function getCountOfMedia(): ?int
    {
        return $this->countOfMedia;
    }

    /**
     * @return Url
     */
    public function getPictureUrl(): ?Url
    {
        return $this->pictureUrl;
    }

    /**
     * @return InstagramProfileUsername
     */
    public function getUsername(): ?InstagramProfileUsername
    {
        return $this->username;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        /* @var static $valueObject */
        return
            parent::isEqualTo($valueObject)
            && $this->getCountOfFollowers() === $valueObject->getCountOfFollowers()
            && $this->getCountOfFollows() === $valueObject->getCountOfFollows()
            && $this->getCountOfMedia() === $valueObject->getCountOfMedia()
            && $this->getPictureUrl() === $valueObject->getPictureUrl()
            && $this->getUsername() === $valueObject->getUsername()
        ;
    }
}
