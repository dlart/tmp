<?php

namespace Todomer\Social\Instagram\Profile;

use Todomer\Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Todomer\Core\Identity\IdentityInterface;
use Todomer\Core\Moment;
use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity as Identity;

/**
 * InstagramProfileSnapshot.
 */
class InstagramProfileSnapshot implements AggregateRoot
{
    /**
     * @var Identity
     */
    private $identity;

    /**
     * @var InstagramProfileMetrics
     */
    private $instagramProfileMetrics;

    /**
     * @var Moment
     */
    private $moment;

    /**
     * @param Identity         $identity
     * @param InstagramProfile $instagramProfile
     *
     * @return InstagramProfileSnapshot
     */
    public static function snapFromInstagramProfile(
        Identity $identity,
        InstagramProfile $instagramProfile
    ): InstagramProfileSnapshot {
        return new self(
            $identity,
            $instagramProfile->getMetrics(),
            Moment::createFromNow()
        );
    }

    /**
     * @param Identity                $identity
     * @param InstagramProfileMetrics $instagramProfileMetrics
     * @param Moment                  $moment
     */
    private function __construct(
        Identity $identity,
        InstagramProfileMetrics $instagramProfileMetrics,
        Moment $moment
    ) {
        $this->identity = $identity;
        $this->instagramProfileMetrics = $instagramProfileMetrics;
        $this->moment = $moment;
    }

    /**
     * @return IdentityInterface
     */
    public function getIdentity(): IdentityInterface
    {
        return $this->identity;
    }

    /**
     * @return InstagramProfileMetrics
     */
    public function getInstagramProfileMetrics(): InstagramProfileMetrics
    {
        return $this->instagramProfileMetrics;
    }

    /**
     * @return Moment
     */
    public function getMoment(): Moment
    {
        return $this->moment;
    }
}
