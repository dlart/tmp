<?php

namespace Todomer\Social\Instagram\Profile;

use Assert\Assertion;
use Todomer\Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * InstagramProfileId.
 */
class InstagramProfileId extends AbstractValueObject implements CanBeCastedToString
{
    private $id;

    /**
     * @param $id
     */
    public function __construct($id)
    {
        if (is_int($id)) {
            Assertion::integer($id);
        } else {
            Assertion::digit($id);
        }

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return
            parent::isEqualTo($valueObject)
            && (string) $this === (string) $valueObject
        ;
    }
}
