<?php

namespace Todomer\Social\Instagram\Profile\Metrics\Provider;

use Todomer\Social\Instagram\Profile\InstagramProfileId;
use Todomer\Social\Instagram\Profile\InstagramProfileMetrics;

/**
 * InstagramProfileMetricsProviderInterface.
 */
interface InstagramProfileMetricsProviderInterface
{
    /**
     * @param InstagramProfileId $instagramProfileId
     *
     * @return InstagramProfileMetrics
     */
    public function getByInstagramProfileId(
        InstagramProfileId $instagramProfileId
    ): InstagramProfileMetrics;
}
