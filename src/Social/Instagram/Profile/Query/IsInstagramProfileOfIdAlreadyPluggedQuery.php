<?php

namespace Todomer\Social\Instagram\Profile\Query;

use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Social\Instagram\Profile\InstagramProfileId;

/**
 * IsInstagramProfileOfIdAlreadyPluggedQuery.
 */
class IsInstagramProfileOfIdAlreadyPluggedQuery implements Query
{
    /**
     * @var InstagramProfileId
     */
    private $instagramProfileId;

    /**
     * IsInstagramProfileOfIdAlreadyPluggedInQuery constructor.
     *
     * @param InstagramProfileId $instagramProfileId
     */
    public function __construct(InstagramProfileId $instagramProfileId)
    {
        $this->instagramProfileId = $instagramProfileId;
    }

    /**
     * @return InstagramProfileId
     */
    public function getInstagramProfileId(): InstagramProfileId
    {
        return $this->instagramProfileId;
    }
}
