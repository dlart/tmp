<?php

namespace Todomer\Social\Instagram\Profile\Query;

use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * IsInstagramProfileOfIdentityExistQuery.
 */
class IsInstagramProfileOfIdentityExistQuery implements Query
{
    /**
     * @var InstagramProfileIdentity
     */
    private $instagramProfileIdentity;

    /**
     * @param InstagramProfileIdentity $instagramProfileIdentity
     */
    public function __construct(InstagramProfileIdentity $instagramProfileIdentity)
    {
        $this->instagramProfileIdentity = $instagramProfileIdentity;
    }

    /**
     * @return InstagramProfileIdentity
     */
    public function getInstagramProfileIdentity(): InstagramProfileIdentity
    {
        return $this->instagramProfileIdentity;
    }
}
