<?php

namespace Todomer\Social\Instagram\Profile\Query\Result;

use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;

/**
 * IsInstagramProfileOfIdentityExistQueryResult.
 */
class IsInstagramProfileOfIdentityExistQueryResult implements QueryResult
{
    /**
     * @var bool
     */
    private $exist;

    /**
     * @param bool $exist
     */
    public function __construct(bool $exist)
    {
        $this->exist = $exist;
    }

    /**
     * @return bool
     */
    public function isExist(): bool
    {
        return $this->exist;
    }
}
