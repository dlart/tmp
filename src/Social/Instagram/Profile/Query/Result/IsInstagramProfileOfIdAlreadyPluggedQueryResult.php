<?php

namespace Todomer\Social\Instagram\Profile\Query\Result;

use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;

/**
 * IsInstagramProfileOfIdAlreadyPluggedQueryResult.
 */
class IsInstagramProfileOfIdAlreadyPluggedQueryResult implements QueryResult
{
    /**
     * @var bool
     */
    private $alreadyPlugged;

    /**
     * @param bool $alreadyPlugged
     */
    public function __construct(bool $alreadyPlugged)
    {
        $this->alreadyPlugged = $alreadyPlugged;
    }

    /**
     * @return bool
     */
    public function isAlreadyPlugged(): bool
    {
        return $this->alreadyPlugged;
    }
}
