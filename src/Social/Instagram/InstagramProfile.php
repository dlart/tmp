<?php

namespace Todomer\Social\Instagram;

use Todomer\Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Todomer\Core\Identity\IdentityInterface;
use Todomer\Social\Instagram\Profile\InstagramProfileId as Id;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity as Identity;
use Todomer\Social\Instagram\Profile\InstagramProfileMetrics as Metrics;

/**
 * InstagramProfile.
 */
class InstagramProfile implements AggregateRoot
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var Identity
     */
    private $identity;

    /**
     * @var Metrics
     */
    private $metrics;

    /**
     * @param Identity $identity
     * @param Id       $id
     *
     * @return InstagramProfile
     */
    public static function plug(Identity $identity, Id $id)
    {
        return new self($identity, $id);
    }

    /**
     * @param Identity $identity
     * @param Id       $id
     */
    private function __construct(Identity $identity, Id $id)
    {
        $this->identity = $identity;
        $this->id = $id;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return IdentityInterface
     */
    public function getIdentity(): IdentityInterface
    {
        return $this->identity;
    }

    /**
     * @return Metrics
     */
    public function getMetrics(): Metrics
    {
        return $this->metrics;
    }

    /**
     * @param Metrics $metrics
     */
    public function updateMetrics(Metrics $metrics): void
    {
        if ($this->metrics && $this->metrics->isEqualTo($metrics)) {
            return;
        }

        $this->metrics = $metrics;
    }
}
