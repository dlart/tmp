<?php

use Composer\Autoload\ClassLoader;
use Symfony\Component\HttpFoundation\Request;

/** @var ClassLoader $classLoader */
$classLoader = require __DIR__.'/../app/autoload.php';

include_once __DIR__.'/../var/bootstrap.php.cache';

$appKernel = new AppKernel('prod', false);

$appKernel->loadClassCache();

$request = Request::createFromGlobals();

$response = $appKernel->handle($request);

$response->send();

$appKernel->terminate($request, $response);
